# GrumPHP Drupal Checker
---

## Overview

Provides an [GrumPHP](https://github.com/phpro/grumphp) configuration for your Drupal project.

## Installation

1. Install GrumPHP for Drupal with composer.
```
composer require --dev jenue/grumphp-drupal-checker
```

2. Copy GrumPHP settings file into your project root directory.
```
cp vendor/jenue/grumphp-drupal-checker/grumphp.yml.dist grumphp.yml
```

Note : During the installation, GrumPHP ask you to create a conf file, select a random task and validate.

That's it.

When the package is installed, GrumPHP will attach itself to the git hooks of your project and ready to sniff your commits.

## Tasks in order

1. [PHPCS](https://github.com/phpro/grumphp/blob/master/doc/tasks/phpcs.md)
2. [PHPLINT](https://github.com/phpro/grumphp/blob/master/doc/tasks/phplint.md)
2. [YAMLLINT](https://github.com/phpro/grumphp/blob/master/doc/tasks/yamllint.md)
3. [COMPOSER](https://github.com/phpro/grumphp/blob/master/doc/tasks/composer.md)
8. [JSONLINT](https://github.com/phpro/grumphp/blob/master/doc/tasks/jsonlint.md)
4. [PHPSTAN](https://github.com/phpro/grumphp/blob/master/doc/tasks/phpstan.md)
5. [TWIGCS](https://github.com/phpro/grumphp/blob/master/doc/tasks/twigcs.md)
6. [DRUPAL CHECK](https://github.com/mglaman/drupal-check)
7. [GIT COMMIT MESSAGE](https://github.com/phpro/grumphp/blob/master/doc/tasks/git_commit_message.md)

